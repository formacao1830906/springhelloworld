#builder
FROM maven:3-ibm-semeru-21-jammy as builder
WORKDIR /src
COPY . .
RUN mvn -f spring-hello-world/ clean package
RUN mv spring-hello-world/target/spring-*.jar spring-hello-world/target/app.jar


# deployment
FROM maven:3-ibm-semeru-21-jammy
WORKDIR /src
ARG APP_DEPLOY_MODE="production"
ENV APP_DEPLOY_MODE=${APP_DEPLOY_MODE}
COPY --from=builder "/src/spring-hello-world/target/app.jar" .
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar", "--spring.profiles.active=${APP_DEPLOY_MODE}" ] 
